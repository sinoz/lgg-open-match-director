import * as sessionApi from "@gameye/match-api-spec";
import { Config } from "./config.js";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface Services {
    gameyeApi: sessionApi.Client;
}

export function createServices(
    config: Config,
): Services {
    const gameyeApi = new sessionApi.Client({
        baseUrl: config.gameyeApiUrl,
        httpSendReceive: config.secureHttpSendReceive,
    }, {
        apiToken: config.gameyeApiKey,
    });

    return {
        gameyeApi,
    };
}
