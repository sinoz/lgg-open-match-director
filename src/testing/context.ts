import * as sessionApi from "@gameye/match-api-spec";
import * as openMatchBackendApi from "@latency.gg/lgg-open-match-backend-oas";
import * as http from "http";
import { Registry } from "prom-client";
import { Promisable } from "type-fest";
import * as application from "../application/index.js";

export interface TestContext {
    appContext: application.Context,
    endpoints: {
        gameyeApi: URL,
        openMatchBackend: URL;
    },
    servers: {
        gameyeApi: sessionApi.Server,
        openMatchBackend: openMatchBackendApi.Server,
    },
}

export async function withContext(
    job: (context: TestContext) => Promisable<void>,
) {
    const endpoints = {
        gameyeApi: new URL("http://localhost:9000"),
        openMatchBackend: new URL("http://localhost:9001"),
    };

    const abortController = new AbortController();
    const registry = new Registry();

    const applicationConfig: application.Config = {
        gameyeApiKey: "123",
        gameyeApiUrl: endpoints.gameyeApi,

        openMatchBackendUrl: endpoints.openMatchBackend,

        abortController,
        promRegistry: registry,
    };

    const onWarning = (error: unknown) => {
        if (!abortController.signal.aborted) {
            throw error;
        }
    };

    const applicationContext = application.createContext(
        applicationConfig,
    );

    const servers = {
        gameyeApi: new sessionApi.Server({
        }),
        openMatchBackend: new openMatchBackendApi.Server({
        }),
    };

    const httpServers = {
        gameyeApi: http.createServer(servers.gameyeApi.asRequestListener({
            onError: onWarning,
        })),
        openMatchBackend: http.createServer(servers.openMatchBackend.asRequestListener({
            onError: onWarning,
        })),
    };

    const context = {
        appContext: applicationContext,
        endpoints,
        servers,
    };

    const keys = Object.keys(endpoints) as Array<keyof typeof endpoints>;
    await Promise.all(
        keys.map(async key => new Promise<void>(resolve => httpServers[key].listen(
            endpoints[key].port,
            () => resolve(),
        ))),
    );
    try {
        await job(context);
    }
    finally {
        abortController.abort();

        await Promise.all(
            keys.map(async key => new Promise<void>((resolve, reject) => httpServers[key].close(
                error => error ?
                    reject(error) :
                    resolve(),
            ))),
        );
    }

}
