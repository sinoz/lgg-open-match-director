import * as sessionApi from "@gameye/match-api-spec";
import * as openMatchBackendApi from "@latency.gg/lgg-open-match-backend-oas";
import { HttpSendReceive } from "@oas3/http-send-receive";
import { assertAbortSignal } from "abort-tools";
import delay from "delay";
import createHttpError from "http-errors";
import { v4 as uuidv4 } from "uuid";
import * as application from "../application/index.js";

const matchProfileSchema: openMatchBackendApi.OpenmatchMatchProfileSchema = {
    name: "1v1",
    pools: [
        {
            name: "Everyone",
        },
    ],
};

interface SessionAssignment {
    matchId: string;
    info: sessionApi.SessionRun201ApplicationJsonResponseSchema;
    tickets: openMatchBackendApi.OpenmatchTicketSchema[];
}

export interface Config {
    openMatchBackendUrl: URL;

    openMatchFunctionUrl: string;
    openMatchFunctionPort: number;

    fetchMatchDelay: number;

    region: string;
    image: string;

    httpSendReceive?: HttpSendReceive;
}

export async function runLoop(
    context: application.Context,
    config: Config,
    signal: AbortSignal,
    onError: (error: any) => void,
) {
    const openMatchBackend = createOpenMatchBackendClient(
        config.openMatchBackendUrl,
        config.httpSendReceive!,
    );

    while (!signal.aborted) {
        try {
            await runOnce(
                openMatchBackend,
                context,
                config,
                signal,
                onError,
            );
        } catch (e) {
            onError(e);
        }

        await delay(config.fetchMatchDelay, { signal });
    }

    assertAbortSignal(signal, "worker loop aborted");
}

export function createOpenMatchBackendClient(
    url: URL,
    httpSendReceive: HttpSendReceive,
) {
    const client = new openMatchBackendApi.Client({
        baseUrl: url,
        httpSendReceive: httpSendReceive,
    });

    client.registerMiddleware(async (request, next) => {
        const response = await next(request);

        if (response.headers["content-type"] == null) {
            response.headers["content-type"] = "application/json";
        }

        return response;
    });

    return client;
}

export async function runOnce(
    client: openMatchBackendApi.Client,
    context: application.Context,
    config: Config,
    signal: AbortSignal,
    onError: (error: any) => void,
) {
    const stream = fetchMatches(
        client,
        config,
        signal,
    );

    const batch = await createAssignmentBatch(
        context,
        config,
        signal,
        stream,
        onError,
    );

    await assignTickets(client, context, batch);
}

async function assignTickets(
    client: openMatchBackendApi.Client,
    context: application.Context,
    assignments: SessionAssignment[],
) {
    const assignmentGroups = assignments.map(v => ({
        ticket_ids: v.tickets.map(t => t.id!),
        assignment: {
            connection: JSON.stringify(v.info),
        },
    }));

    const response = await client.backendServiceAssignTickets({
        parameters: {},
        entity() {
            return {
                assignments: assignmentGroups,
            };
        },
    });

    if (response.status !== 200) {
        throw createHttpError(response.status, `failed to assign tickets: ${assignmentGroups}`);
    }

    return await response.entity();
}

async function createAssignmentBatch(
    context: application.Context,
    config: Config,
    signal: AbortSignal,
    // eslint-disable-next-line max-len
    stream: AsyncIterable<openMatchBackendApi.BackendServiceFetchMatches200ApplicationJsonResponseSchema>,
    onError: (error: any) => void,
) {
    const assignments: SessionAssignment[] = [];
    for await (const { result, error } of stream) {
        if (signal.aborted) break;

        if (error) {
            onError(JSON.stringify(error));
            continue;
        }

        const match = result?.match;
        if (!match) {
            onError(new Error("no match included"));
            continue;
        }

        const tickets = match.tickets;
        if (!tickets || tickets.length === 0) {
            onError(new Error(`no tickets in match ${match.match_id}`));
            continue;
        }

        const matchId = match.match_id!;
        const sessionId = uuidv4();

        // for now for the sake of simplicity, one session at a time is to
        // be ignited. in the future, this should run concurrently to allow
        // this loop to ignite many at once

        const sessionInfo = await startSession(
            context,
            sessionId,
            config.region,
            config.image,
        );

        assignments.push({
            matchId,
            info: sessionInfo,
            tickets,
        });
    }

    return assignments;
}

async function startSession(
    context: application.Context,
    sessionId: string,
    region: string,
    image: string,
): Promise<sessionApi.SessionRun201ApplicationJsonResponseSchema> {
    const response = await context.services.gameyeApi.sessionRun({
        parameters: {},
        entity() {
            return {
                id: sessionId,
                location: region,
                image,
                env: {},
                args: [],
                restart: false,
            };
        },
    });

    if (response.status !== 201) {
        throw createHttpError(response.status, "could not start a session");
    }

    return await response.entity();
}

async function* fetchMatches(
    client: openMatchBackendApi.Client,
    config: Config,
    signal: AbortSignal,
) {
    const matchFunctionSchema: openMatchBackendApi.OpenmatchFunctionConfigSchema = {
        host: config.openMatchFunctionUrl,
        port: config.openMatchFunctionPort,
        type: "GRPC",
    };

    const request = {
        config: matchFunctionSchema,
        profile: matchProfileSchema,
    } as openMatchBackendApi.OpenmatchFetchMatchesRequestSchema;

    const response = await client.backendServiceFetchMatches({
        parameters: {},
        async entity() {
            return request;
        },
    });

    if (response.status !== 200) {
        throw createHttpError(response.status, "failed to fetch matches");
    }

    yield* response.entities(signal);
}
